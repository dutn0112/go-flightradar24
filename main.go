package main

import (
	"fmt"
	"gitlab.com/dutn0112/go-flightradar24/flightradar24"
)

func main() {
	flightId := "VN594"

	data, err := flightradar24.GetRawFlight(flightId)
	if err != nil {
		fmt.Errorf("%v", err)
	}
	fmt.Println(string(data))

	flightNumber := "CX1762"
	data2, err2 := flightradar24.GetRawFlightCheck(flightNumber)
	if err2 != nil {
		fmt.Errorf("%v", err2)
	}
	fmt.Println(string(data2))

	airports, airportError := flightradar24.GetAirports()
	if airportError != nil {
		fmt.Errorf("%v", airportError)
	}
	fmt.Println(string(airports))
}
