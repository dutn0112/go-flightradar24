package flightradar24

import "fmt"

var baseUrl = "https://www.flightradar24.com"
var apiUrl = "https://api.flightradar24.com/common/v1"

var metadataEndPoints = map[string]string{
	"airports": "/_json/airports.php",
	"airlines": "/_json/airlines.php",
}

var realTimeDataEndPoints = map[string]string{
	"flight": "/flight/list.json?&fetchBy=flight&page=1&limit=25&query=", // add flight number e.g: VN594
}

/**
 * Get airports
 */
func GetAirports() ([]byte, error) {
	endpoint := baseUrl + metadataEndPoints["airports"]
	return RequestApi(endpoint)
}

/**
 * Get airlines
 */
func GetAirlines() ([]byte, error) {
	endpoint := baseUrl + metadataEndPoints["airlines"]
	return RequestApi(endpoint)
}

/**
 *	Check a flight. e.g CX1762
 */
func GetRawFlightCheck(flightNumber string) ([]byte, error) {
	endpoint := fmt.Sprintf("https://www.flightradar24.com/v1/search/web/find?query=%s&limit=18&type=schedule", flightNumber)
	return RequestApi(endpoint)
}

/**
 *	Get raw data of a flight. e.g VN594
 */
func GetRawFlight(flightId string) ([]byte, error) {
	endpoint := apiUrl + realTimeDataEndPoints["flight"] + flightId
	return RequestApi(endpoint)
}
