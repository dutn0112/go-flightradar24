package flightradar24

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func RequestApi(endpoint string) ([]byte, error) {
	resp, err := http.Get(endpoint)

	if err != nil {
		return nil, err
	}

	if resp.StatusCode == 402 {
		return nil, fmt.Errorf("requerst to %s requires payment", endpoint)
	}

	if resp.StatusCode == 403 {
		return nil, fmt.Errorf("request to %s is Forbidden", endpoint)
	}

	if resp.StatusCode == 404 {
		return nil, fmt.Errorf("request to %s is NotFound", endpoint)
	}

	if resp.StatusCode == 500 {
		return nil, fmt.Errorf("request to %s return InternalServerError", endpoint)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("read response data error %v", err)
	}
	return data, nil
}
