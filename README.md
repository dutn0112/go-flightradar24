Data library for Flightadar24.com written in Golang.

If you want to use  the data collected using this library commercially, You need to subscribe to Business Plan. See details at  `https://www.flightradar24.com/premium/ <https://www.flightradar24.com/premium/>`_

Installation
============

    go get gitlab.com/dutn0112/go-flightradar24

Getting airports list
    
    import "gitlab.com/dutn0112/go-flightradar24/flightradar24"
    
    airports, err := flightradar24.GetAirports()
    
Getting airlines list
    
    import "gitlab.com/dutn0112/go-flightradar24/flightradar24"
    
    airlines, err := flightradar24.GetAirlines()

Getting flight id

    import "gitlab.com/dutn0112/go-flightradar24/flightradar24"
    
    flightNumber := "CX1762"
    
    data, err := flightradar24.GetRawFlightCheck(flightNumber)
    
    

Getting flight details

    import "gitlab.com/dutn0112/go-flightradar24/flightradar24"
    
    flightId := "VN594"
    
    data, err := flightradar24.GetRawFlight(flightId)
    